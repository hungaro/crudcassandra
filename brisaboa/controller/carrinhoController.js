const cassandra = require("cassandra-driver");


var client = new cassandra.Client({contactPoints: ['localhost'], localDataCenter: 'datacenter1', keyspace: 'ecommercie' });
client.connect(function(err, result){
  console.log('cassandra conectou carrinho');
});


exports.cadastro = function (req, res) {

    id_produto = req.body["id"];
    nome = req.body["nome"];
    valorUnitario = req.body["preco"];

    console.log(typeof(id_produto))
    console.log(typeof(nome))
    console.log(typeof(valorUnitario))

    var cadastrar = "INSERT INTO carrinho(id_carrinho, id_produto, nome_produto, valor_unitario) values(uuid(),?, ?, ?);";


    client.execute(cadastrar, [ id_produto, nome, parseFloat(valorUnitario) ], { prepare: true }, function (error, result) {
        if(error){
            console.log(error);
            
            res.status(400).send({msg:"Nao foi possivel inserir registro"});
        }else{
            res.status(200).send({msg:"Registro inserido com sucesso"});
        } 
    });
};

exports.listaCarrinho = function (req, res) {
    var listarCarrinho = "SELECT * FROM carrinho;";

    client.execute(listarCarrinho, []).then(result => res.status(200).send(result.rows));
}


// exports.procuraIdProduto =  function (req, res) {
    
//     const id = req.params.t;
//     var listarId = "SELECT * FROM produtos WHERE id_produto = ?;";

//     // client.execute(listarId, [id]).then(result => console.log(result.rows[0]));

//     client.execute(listarId, [id]).then(result => res.status(200).send(result.rows[0]));
// }

// exports.editar = function(req, res) {

//     nome = req.body["nome"];
//     preco = req.body["preco"];
//     descricao = req.body["descricao"];
//     id = req.body["id"];

//     var edit = "INSERT INTO produtos (id_produto, nome_produto, preco_produto, descricao_produto) VALUES (?, ?, ?, ?);";

//     client.execute(edit, [id, nome, preco, descricao], function (error, result) {
//         if(error){
//             console.log(error);
//             res.status(400).send({msg:"Nao foi possivel editar o registro"});
//         }else{
//             res.status(200).send({msg:"Registro editado com sucesso"});
//         } 
//     });
// };

exports.deletar = function (req, res) {
    
    id = req.params.id;
    

    var delCarrinho = "DELETE FROM carrinho WHERE id_carrinho = ?;";
    client.execute(delCarrinho, [id], function (error, result) {
        if (error) {
            res.status(400).send({ msg: "Nao foi possivel excluir o registro" });
        } else {
            res.status(200).send({ msg: "Registro excluido com sucesso" });
        }
    }); 
}