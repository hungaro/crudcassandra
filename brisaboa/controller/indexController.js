const cassandra = require("cassandra-driver");
const crypto = require("crypto");

var client = new cassandra.Client({contactPoints: ['localhost'], localDataCenter: 'datacenter1', keyspace: 'ecommercie' });
client.connect(function(err, result){
  console.log('cassandra conectou');
});


exports.cadastro = function (req, res) {

    nome = req.body["nome"];
    preco = req.body["preco"];
    descricao = req.body["descricao"];

    var cadastrar = "INSERT INTO ecommercie.produtos (id_produto, nome_produto, preco_produto, descricao_produto) VALUES (uuid(), ?, ?, ?);";

    client.execute(cadastrar, [ nome, parseFloat(preco), descricao], function (error, result) {
        if(error){
            console.log(error);
            res.status(400).send({msg:"Nao foi possivel inserir registro"});
        }else{
            res.status(200).send({msg:"Registro inserido com sucesso"});
        } 
    });
};

exports.listaProduto = function (req, res) {
    var listarProduto = "SELECT * FROM produtos;";

    client.execute(listarProduto, []).then(result => res.status(200).send(result.rows));
}

exports.procuraIdProduto =  function (req, res) {
    
    const id = req.params.t;
    var listarId = "SELECT * FROM produtos WHERE id_produto = ?;";

    client.execute(listarId, [id]).then(result => res.status(200).send(result.rows[0]));
}

exports.editar = function(req, res) {

    nome = req.body["nome"];
    preco = req.body["preco"];
    descricao = req.body["descricao"];
    id = req.params.id;

    var edit = "INSERT INTO produtos (id_produto, nome_produto, preco_produto, descricao_produto) VALUES (?, ?, ?, ?);";

    client.execute(edit, [id, nome, preco, descricao], function (error, result) {
        if(error){
            console.log(error);
            res.status(400).send({msg:"Nao foi possivel editar o registro"});
        }else{
            res.status(200).send({msg:"Registro editado com sucesso"});
        } 
    });
};

exports.deletar = function (req, res) {
    
    id = req.params.id;
    
    var deleUsuario = "DELETE FROM produtos WHERE id_produto = ?;";
    client.execute(deleUsuario, [id], function (error, result) {
        if (error) {
            res.status(400).send({ msg: "Nao foi possivel excluir o registro" });
        } else {
            res.status(200).send({ msg: "Registro excluido com sucesso" });
        }
    }); 
}