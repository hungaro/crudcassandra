const cassandra = require("cassandra-driver");
const crypto = require("crypto");
const express = require("express");
const route = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const authConfig = require("../config/auth");

var client = new cassandra.Client({ contactPoints: ['localhost'], localDataCenter: 'datacenter1', keyspace: 'ecommercie' });
client.connect(function (err, result) {
    console.log('cassandra conectou');
});

exports.login = function (req, res) {
    email = req.body["email"];
    senha = returnHash(req.body["senha"]);

    var LoginV = "SELECT * FROM  usuarios WHERE email_usuario = ? AND senha_usuario = ? ALLOW FILTERING;";
    client.execute(LoginV, [email, senha],{ prepare : true }, function (error, result) {
        console.log(error);
        if (result.rows[0].length === 0) {
            res.status(404).send({ msg: "Usuario não encontrado" });
        } else {
            
            const token = jwt.sign({ id: result.rows[0].id_usuario, tipo: result.rows[0].tipo }, authConfig.secret, {
                expiresIn: 86400,
            });
            res.send({token});
        }

    });
}

exports.cadastro = function (req, res) {

    
    nome = req.body["nome"];
    email = req.body["email"];
    endereco = req.body["endereco"];
    numcasa = req.body["numCasa"];
    bairro = req.body["bairro"];
    senha = returnHash(req.body["senha"]);
    tipo = "cliente";

    var cadUssuario = "INSERT INTO ecommercie.usuarios ( id_usuario, bairro_usuario, email_usuario, enderco_usuario, nome_usuario, numcasa_usuario, senha_usuario, tipo) VALUES (uuid(), ?, ?, ?, ?, ?, ?, ?);";

    client.execute(cadUssuario, [ nome, email, endereco, numcasa, bairro, senha, tipo], function (error, result) {
        if (error) {
            res.status(400).send({ msg: "Nao foi possivel inserir registro" });
        } else {
            res.status(200).send({ msg: "Registro inserido com sucesso" });
        }
    });
};


exports.listaUsuario = function (req, res) {
    var lista = "SELECT * FROM usuarios;";

    client.execute(lista, []).then(result => res.status(200).send(result.rows));
}

exports.editarUsuario = function (req, res) {

    id = req.body["id"];
    nome = req.body["nome"];
    email = req.body["email"];
    endereco = req.body["endereco"];
    numcasa = req.body["numCasa"];
    bairro = req.body["bairro"];
    senha = returnHash(req.body["senha"]);
    tipo = req.body["tipo"];

    var editar = "INSERT INTO ecommercie.usuarios ( id_usuario, bairro_usuario, email_usuario, enderco_usuario, nome_usuario, numcasa_usuario, senha_usuario, tipo) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

    client.execute(editar, [id, nome, email, endereco, numcasa, bairro, senha, tipo], function (error, result) {
        if (error) {
            res.status(400).send({ msg: "Nao foi possivel editar o registro" });
        } else {
            res.status(200).send({ msg: "Registro editado com sucesso" });
        }
    });
}

exports.deletar = function (req, res) {
    
    id = req.params.id;
    
    var deleUsuario = "DELETE FROM usuarios WHERE id_usuario = ?;";
    client.execute(deleUsuario, [id], function (error, result) {
        if (error) {
            res.status(400).send({ msg: "Nao foi possivel excluir o registro" });
        } else {
            res.status(200).send({ msg: "Registro excluido com sucesso" });
        }
    }); 
}

function returnHash(senha) {
    let salt = "... salada saladinha bem temperadinha com sal pimenta fogo foguinho ...";
    senha += senha.split('').reverse().join();
    senha = crypto.createHash("ripemd160WithRSA").update(salt + senha).digest('hex');
    return crypto.createHash("whirlpool").update(senha).digest('hex');
}