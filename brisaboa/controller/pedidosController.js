const cassandra = require("cassandra-driver");

var client = new cassandra.Client({ contactPoints: ['localhost'], localDataCenter: 'datacenter1', keyspace: 'ecommercie' });
client.connect(function (err, result) {
  console.log('cassandra conectou');

});


exports.cadastro = function (req, res) {

  now = new Date();

  id_usuario = req.body["id_usuario"];
  id_produto = req.body["id_produto"];
  quantidade = req.body["quantidado"];
  formaPagamento = req.body["pagamento"];
  valor_total = req.body["valorTotal"];
  data = now.getFullYear() + '-' + now.getMonth() + '-' + now.getDate();

  var cadastrar = "INSERT INTO pedidos ( id_pedido, data_pedido, forma_pagamento, id_produto, id_usuario, quantidade, valor_total) VALUES (uuid()7, ?, ?, ?, ?, ?, ?);";

  client.execute(cadastrar, [ data, formaPagamento, id_produto, id_usuario, Number(quantidade), parseFloat(valor_total)], function (error, result) {
    if (error) {
      console.log(error);
      res.status(400).send({ msg: "Nao foi possivel inserir registro" });
    } else {
      res.status(200).send({ msg: "Registro inserido com sucesso" });
    }
  });
};



exports.listarPedidos = function (req, res) {

  var pedidos = "SELECT * FROM pedidos;";
  client.execute(pedidos, []).then(result => res.status(200).send(result.rows));
}


exports.procuraIdPedido = function (req, res) {

  id = req.params.id;
  var listarId = "SELECT * FROM pedidos WHERE id_pedido = ?;";

  client.execute(listarId, [id], { prepare: true }).then(
    result => console.log(result.rows[0]));
}



