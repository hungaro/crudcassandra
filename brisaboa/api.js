const express = require("express");
const bodyParser = require('body-parser');
const cors = require("cors");

// Import Rotas;
const indexRota = require("./routes/indexRoutes");
const usuarioRota = require("./routes/usuarioRoutes");
const pedidosRota = require("./routes/pedidosRoutes");
const carrinhoRota = require("./routes/carrinhoRoutes")

const api = express();
api.use(cors());

// Recursos;
api.use(bodyParser.json()); // support json encoded bodies
api.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Rotas;
api.use("/", indexRota);
api.use("/usuario", usuarioRota);
api.use("/pedidos", pedidosRota);
api.use("/carrinho", carrinhoRota);

api.listen(3000, log => {
    
    console.log("Api rodando");
});


