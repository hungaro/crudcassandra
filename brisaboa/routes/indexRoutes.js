const express = require("express");
const router = express.Router();
const cors = require("cors");

const controller = require("../controller/indexController");

router.post("/cadastro", controller.cadastro);
router.post("/editarProduto/:id", controller.editar);

router.get("/listaProduto", controller.listaProduto);
router.get("/listaId/:t", controller.procuraIdProduto);

router.delete("/deletarProduto/:id", controller.deletar);

module.exports = router;