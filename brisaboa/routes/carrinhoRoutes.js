const express = require("express");
const router = express.Router();

const controller = require("../controller/carrinhoController");

router.post("/adicionar", controller.cadastro)

router.get("/listaCarrinho", controller.listaCarrinho);

router.delete("/deletarItemCarrinho/:id", controller.deletar);

module.exports = router;