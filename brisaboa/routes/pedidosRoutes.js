const express = require("express");
const router = express.Router();

const controller = require("../controller/pedidosController");

router.post("/cadastro", controller.cadastro);

router.get("/listaPedidos", controller.listarPedidos);
router.get("/listaId/:id", controller.procuraIdPedido);

module.exports = router;