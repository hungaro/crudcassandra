const express = require("express");
const router = express.Router();

const controller = require("../controller/usuarioController");

router.post("/login", controller.login);
router.post("/UserCadastro",controller.cadastro);
router.get("/listaUsuario", controller.listaUsuario);
router.post("/editarUsuario/:id", controller.editarUsuario);
router.delete("/deletarUsuario/:id", controller.deletar);

module.exports = router;