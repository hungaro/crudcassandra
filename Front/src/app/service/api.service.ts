import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { Produto } from '../model/model';
import { User } from '../model/user';
import { Usuario } from '../model/usuario';
import { Pedido } from '../model/pedido';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  [x: string]: any;
  private API_URL = 'http://localhost:3000/';
  constructor(
    private http: HttpClient
  ) {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }


  getAllProdutos<T>(): Observable<T> {
    return this.http.get<T>(this.API_URL + 'listaProduto');
  }

  login(email: string, senha: string): Observable<any> {
    return this.http.post<any>(this.API_URL + 'usuario/login', { email: email, senha: senha })
      .pipe(
        map(user => {
          // login bem-sucedido se houver um token jwt na resposta
          if (user && user.token) {
            // armazenar detalhes do usuário e token jwt no localStorage para manter o usuário logado entre as atualizações da página
            localStorage.setItem('currentUser', JSON.stringify(user));
          }

          return user;
        })
      );
  }

  cadastroUsuario(nome: string, email: string, endereco: string,
    numCasa: string, bairro: string, senha: string, tipo: string): Observable<User> {
    return this.http.post<User>(this.API_URL + 'usuario/userCadastro', {
      nome: nome,
      email: email,
      endereco: endereco,
      numCasa: numCasa,
      bairro: bairro,
      senha: senha,
      tipo: tipo
    });
  }

  getAllUsuarios<T>(): Observable<T> {
    return this.http.get<T>(this.API_URL + 'usuario/listaUsuario');
  }

  editaUsuario(id: number, nome: string, preco: string, descricao: string): Observable<Usuario> {
    return this.http.post<Usuario>(this.API_URL + 'usuario/editarUsuario/' + id, {
      id: id,
      nome: nome,
      preco: preco,
      descricao: descricao
    });
  }

  deleteUsuario<T>(id: number): Observable<T> {
    return this.http.delete<T>(this.API_URL + 'usuario/deletarUsuario/' + id)
  }

  // CRUD PRODUTO

  cadastroProduto(nome: string, preco: string, descricao: string): Observable<Produto> {
    return this.http.post<Produto>(this.API_URL + 'cadastro', {
      nome: nome,
      preco: preco,
      descricao: descricao
    });
  }

  getAllUsuario<T>(): Observable<T> {
    return this.http.get<T>(this.API_URL + 'usuario/listaUsuario');
  }


  editaProduto(id: number, nome: string, preco: string, descricao: string): Observable<Produto> {
    return this.http.post<Produto>(this.API_URL + 'editarProduto', {
      id: id,
      nome: nome,
      preco: preco,
      descricao: descricao
    });
  }

  deleteProduto<T>(id: number): Observable<T> {
    return this.http.delete<T>(this.API_URL + 'deletarProduto/' + id);
  }

  // cadastrar carrinho

  cadastrarPedido<T>(iduser: number, idprod: number): Observable<Pedido> {
    return this.http.post<Pedido>(this.API_URL + 'pedidos/cadastro', {
      id_produto: idprod,
      id_usuario: iduser,
    });

  }
}

