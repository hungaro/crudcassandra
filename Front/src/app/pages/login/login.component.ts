import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { User } from 'src/app/model/user';
import { Router } from '@angular/router';
//import { RouterModule } from 'src/app/app-routing.module'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: User = new User();
  constructor(
    private apiService: ApiService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  login() {
    this.apiService.login(
      this.model.email,
      this.model.senha
    ).subscribe(
      user => {
        if (user.token) {
          console.log(this.parseJwt(user.token));
          this.router.navigateByUrl('/index');
        }
      },
      err => {
        console.error(err.error);
      });
  }

  parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = decodeURIComponent(atob(base64Url).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(base64);
  };

}
