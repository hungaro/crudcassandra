import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ApiService } from 'src/app/service/api.service';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/model/usuario';

@Component({
  selector: 'app-cadastro-user',
  templateUrl: './cadastro-user.component.html',
  styleUrls: ['./cadastro-user.component.css']
})
export class CadastroUserComponent implements OnInit {
  model: Usuario = new Usuario();
  users: any;
  constructor(private http: Http,
    private apiService: ApiService,
    private router: Router) { }

  ngOnInit() {
    this.getAllusuarios()
  }

  getAllusuarios() {
    this.apiService.getAllUsuarios().subscribe(
      response => {
        this.users = response;
        console.log(this.users);
      },
      err => {
        console.error(err.error);
      });
  }

  cadastroUsuario() {
      this.apiService.cadastroUsuario(
        this.model.nome,
        this.model.email,
        this.model.endereco,
        this.model.numCasa,
        this.model.bairro,
        this.model.senha,
        this.model.tipo,
      ).subscribe(
        usuario => {
          window.location.reload();
          this.router.navigateByUrl('/cadastro-user');
        },
        err => {
          console.error(err.error);
        }
      )
    }

  deleteUsuario(id) {
    return this.apiService.deleteUsuario(id).subscribe(data => {
      window.location.reload();
      this.router.navigateByUrl('/cadastro-user');
    },
      err => {
        console.error(err.error);
      });
  }
}