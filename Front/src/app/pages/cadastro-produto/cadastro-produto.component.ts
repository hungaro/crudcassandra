import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Produto } from 'src/app/model/model';

@Component({
  selector: 'app-cadastro-produto',
  templateUrl: './cadastro-produto.component.html',
  styleUrls: ['./cadastro-produto.component.css']
})
export class CadastroProdutoComponent implements OnInit {
  model: Produto = new Produto();
  users: any;
  constructor(private http: Http,
    private apiService: ApiService,
    private router: Router) { }

  ngOnInit() { this.getAllprodutos()
  }
  getAllprodutos() {
    this.apiService.getAllProdutos().subscribe(
      response => {
        this.users = response;
        console.log(this.users);
      },
      err => {
        console.error(err.error);
      });
  }

  cadastroProduto() {
      this.apiService.cadastroProduto(
        this.model.nome,
        this.model.preco,
        this.model.descricao,
      ).subscribe(
        usuario => {
          window.location.reload();
          this.router.navigateByUrl('/cadastro-produto');
        },
        err => {
          console.error(err.error);
        }
      )
    }

  deleteProduto(id) {
    return this.apiService.deleteProduto(id).subscribe(data => {
      window.location.reload();
      this.router.navigateByUrl('/cadastro-produto');
    },
      err => {
        console.error(err.error);
      });
  }
}
