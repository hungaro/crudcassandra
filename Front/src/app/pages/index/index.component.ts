import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from "rxjs/operators";
import { ApiService } from 'src/app/service/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  carrinho = [];
  items: any;
  private apiUrl = 'http://10.64.205.52:3000/'
  data: any = {};
  token = this.parseJwt(localStorage.getItem('currentUser'));

constructor(private http: Http,
  private apiService: ApiService,
  private router: Router) {
  this.getAllprodutos();


}

getAllprodutos() {
  this.apiService.getAllProdutos().subscribe(
    response => {
      this.items = response;
      console.log(this.items);
    },
    err => {
      console.error(err.error);
    });

}

cadastroPedido(idprod, iduser, ) {
  this.apiService.cadastrarPedido(
    idprod, 
    iduser,
  ).subscribe(
    usuario => {
      console.log("fon")
      this.router.navigateByUrl('/checkout');
    },
    err => {
      console.error(err.error);
    }
  )
}


parseJwt(token) {
  var base64Url = token.split('.')[1];
  var base64 = decodeURIComponent(atob(base64Url).split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));

  return JSON.parse(base64);
};

ngOnInit() {
}

}
