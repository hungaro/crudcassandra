export class Usuario {
    nome: string;
    email: string;
    endereco: string;
    numCasa: string;
    bairro: string;
    senha: string;
    tipo: string
}